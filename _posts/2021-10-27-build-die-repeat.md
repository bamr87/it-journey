---
title: Build-die-repeat
author: null
description: null
permalink: 'posts/{{ page.name }}/'
categories:
  - Posts
sidebar:
  nav: docs
toc: true
toc_sticky: true
date: '2021-10-27T21:42:21.287Z'
lastmod: '2021-10-27T21:52:33.833Z'
draft: false
---

The key to progressing your skills in programming, or anything IT related, is to always repeat your creations. this allows you to refine your method, but also reinforce your understanding/memory of the system/dependencies you require. 
