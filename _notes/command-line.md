---
title: Command Line
description: Command Line Notes
collection: notes
permalink: /notes/command-line/
lastmod: '2021-11-06T21:43:26.822Z'
---

# Command Line

## bash

```bash
echo "hello world"
cd ~
```

## powershell
```powershell
write-host "hello world"
```

## github cli

To create a clone of your fork, use the --clone flag.

```cmd
$gh_repo = "bamr87/it-journey"
gh repo fork $gh_repo --clone=true
```

wmic 
/output:C:\Temp\list.txt product get name, version
exit

```powershell

Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force

Install-Script -Name Get-RemoteProgram

cd ~\OneDrive\dev\os\win

Export-StartLayout -UseDesktopApplicationID -Path layout.xml

```
